#!/bin/bash
#include <math.h>

heuredebut=`date +%s.%N | cut -b-12` 
echo "Bienvenue dans le Projet Info 3 sur les relevés météorologiques"

test="exe" #Creation de la variable test qui vérifiera 
if [ -x $test ] #Test qui regarde si l'exécutable est dans le répertoire
then
  echo " $test est présent"
else
  echo "Il n'y a pas l'exécutable" 
  make      # Si il n'y est pas , on make pour le rajouter
fi


g=0  #Index pour savoir si l'argument g: longitude est utilisé
a=0 #Index pour savoir si l'argument a: latitude est utilisé 
d=0 #Index pour savoir si l'argument d: date est utilisé
w=0 #Index pour savoir si l'argument w: vent est utilisé
h=0 #Index pour savoir si l'argument h: hauteur utilisé
m=0 #Index pour savoir si l'argument m: humidite est utilisé
p=0 #Index pour savoir si l'argument p: pression est utilisé
t=0 #Index pour savoir si l'argument t: temperature  utilisé
F=0 #Index pour savoir si l'argument F: France  utilisé
G=0 #Index pour savoir si l'argument G: Guyanne  utilisé
S=0 #Index pour savoir si l'argument S: Saint Pierre et Miquelon  utilisé
A=0 #Index pour savoir si l'argument A: Antilles  utilisé
O=0 #Index pour savoir si l'argument O: Ocean Indien  utilisé
Q=0 #Index pour savoir si l'argument Q: Antartique utilisé

ind=0 #Index pour savoir si l'argument g: longitudeest utilisé
salut=0 #Index pour savoir si l'argument g: longitudeest utilisé
modt=0 #Index pour savoir quel mode de t on veut
modp=0 #Index pour savoir quel mode de p on veut
resopt=0 #Index pour savoir combien d'optionnels on a 
help=0 #Index pour savoir si l'argument h: help est utilisé
i=0 #Index pour savoir si l'argument i: fichier entrant utilisé
o=0 #Index pour savoir si l'argument o: fichier sortant utilisé
nomfinal=0
while getopts ":i: :o: :g: :a: :d: :t: :p: HhmwFGSAOQ" opts; do # Fonction getopts pour récupérer les arguments
  OPTIND=1 
  case ${opts} in
  i)     #Cas ou i est présent
      shift
      fichint=$1 #On récupère le nom du document d'entrée
      i=1 #Puisque présent la valeur passe à 1
      shift
      ;;
  o)
      shift
      fichout=$1 #On récupère le nom du document sortant
      o=1 #Puisque présent la valeur passe à 1
      shift
      ;;  
  g)
      shift
      ming=$1 #On récupère la valeur de ming
      shift   #Puis on shift 
      maxg=$1 #Pour récupérer la valeur de maxg
      shift
      g=1 #Puisque présent la valeur passe à 1
      ;;

  a)
      shift
      mina=$1 #On récupère la valeur de mina
      shift   #Puis on shift
      maxa=$1 #Pour récupérer la valeur de maxa
      shift
      a=1 #Puisque présent la valeur passe à 1
      ;;

  d)
      shift
      mind=$1 #On récupère la valeur de mind
      shift   #Puis on shift
      maxd=$1 #Pour récupérer la valeur de maxd
      shift
      d=1 #Puisque présent la valeur passe à 1
      ;;
  H)
    shift
    help=1 #Puisque présent la valeur passe à 1
    ;;
  h)
    shift 
    h=1 #Puisque présent la valeur passe à 1
    ;;  
  w)
    shift
    w=1 #Puisque présent la valeur passe à 1
    ;;
  m) 
    shift
    m=1 #Puisque présent la valeur passe à 1
    ;;
  t)
    shift
    t=1 #Puisque présent la valeur passe à 1
    modt=$1 #On récupère le mode de t
    shift
    ;;
  p)
    shift
    p=1 #Puisque présent la valeur passe à 1
    modp=$1 #On récupère le mode de p
    shift
    ;;
  F)
    shift
    F=1 
    ;;
  G)  
    shift
    G=1
    ;;
  S)
    shift
    S=1
    ;;
  A)
    shift
    A=1
    ;;
  O)
    shift
    O=1
    ;;
  Q)
    shift
    Q=1
    ;;        
  *)
    # Traitement des arguments non valides ici
    shift # Consomme l'argument non valide
    ;;
  esac
done

resloc=0
resexclu=0
resloc=$(echo "$F+$G+$S+$A+$O+$Q" | bc -l)
resexclu=$(echo "$m+$w+$h+$p+$t+$help" | bc -l) #On addionne les index des options exclusives
resopt=$(echo "$g+$a+$d" | bc -l)   #On addionne les index des options optionnels

if [ $help -eq 1 ] #Le cas où l'utilisateur a besoin du help
then
   echo " Vous avez demandé l'aide détaillé du programme C, le voici :"
   ./exe -H
fi

if [ $i -eq 0 ] #Le cas ou il n'y a aucune pas de fichier d'entrée précisé
then
  echo " Il est obligatoire de spécifier le chemin du fichier CSV d’entrée"
  make clean
  rm exe
  exit 20
fi


if [ $o -eq 0 ] #Le cas ou il n'y a aucune pas de fichier d'entrée précisé
then
  nomfinal=meteo2023.csv
fi

if [ $o -eq 1 ] #Le cas ou il n'y a aucune pas de fichier d'entrée précisé
then
  nomfinal=$fichout
fi

if [ $resloc -gt 1 ]
then 
   echo " Vous ne pouvez avoir qu'au max une localisation défini"
   make clean
   rm exe
   exit 20
fi

if [ $resloc -eq 0 ]
then 
   cat $fichint  > mesdonnees.csv  # Dans le cas ou pas de localisation est donné on récupère tout le fichier
fi

if [ $F -eq 1 ]
then 
   grep '^07' $fichint  > mesdonnees.csv  #Les ID de la France et de la Corse commencent par 07
fi

if [ $G -eq 1 ]
then 
  grep '^814' $fichint  > mesdonnees.csv #Les ID de la Guyanne commencent par 81
fi

if [ $S -eq 1 ]
then 
  grep '^71805' $fichint  > mesdonnees.csv #Les ID de Saint-Pierre et Miquelon commencent par 71805
fi

if [ $A -eq 1 ]
then 
  grep '^78' $fichint > mesdonnees.csv #Les ID des Antilles commenbcent par 78
fi

if [ $O -eq 1 ]
then 
  grep '^6' $fichint  > mesdonnees.csv #Les ID de l'Ocean Indien commencent par 6 ou 71
fi

if [ $Q -eq 1 ]
then 
  grep '^89642' $fichint  > mesdonnees.csv   #Les ID de L'Antartique commencent par 89
fi


if [ $resexclu -eq 0 ] #Le cas ou il n'y a aucune option exclusive
then
  echo " Vous devez obligatoirement utilisé une des options exclusives: t,p,h,w,m ou help "
  make clean
  rm exe
  exit 20
fi

if [ $resexclu -gt 1 ] #Le cas ou il n'y a trop d'options exclusives
then
  echo "Vous ne pouvez utiliser qu'une option exclusive par exécution du script"
  make clean
  rm exe
  exit 20
fi




if [ $resexclu -eq 1 ] #Ici on va prendre en cas tous les cas où il y a un argument exclusif
then 
  if [ $resopt -eq 0 ] #Ici on va prendre en cas tous les cas où il n'y a aucun argument optionnel
  then
      if [ $w -eq 1 ] #Ici l'argument exclusif est w
        then
          ./exe -w -o $nomfinal 
        elif [ $m -eq 1 ] #Ici l'argument exclusif est m
        then 
          ./exe -m -o $nomfinal 
        elif [ $h -eq 1 ]  #Ici l'argument exclusif est h
        then
          ./exe -h -o $nomfinal 
        elif [ $t -eq 1 ] #Ici l'argument exclusif est t
        then 
          ./exe -t $modt -o $nomfinal
        elif [ $p -eq 1 ] #Ici l'argument exclusif est p
          then 
          ./exe -p $modp -o $nomfinal 
      fi 
  elif [ $resopt -eq 1 ] #Ici on va prendre en cas tous les cas où il y a un argument optionnel
  then
    if [ $a -eq 1 ] #Ici l'argument optionnel est a
      then
        if [ $w -eq 1 ] #Ici l'argument exclusif est w
        then
          ./exe -w -a $mina $maxa -o $nomfinal 
        elif [ $m -eq 1 ]  #Ici l'argument exclusif est m
        then 
          ./exe -m -a $mina $maxa -o $nomfinal 
        elif [ $h -eq 1 ]  #Ici l'argument exclusif est h
        then 
          ./exe -h -a $mina $maxa -o $nomfinal 
        elif [ $t -eq 1 ] #Ici l'argument exclusif est t
        then 
          ./exe -t $modt -a $mina $maxa -o $nomfinal 
        elif [ $p -eq 1 ] #Ici l'argument exclusif est p
          then 
          ./exe -p $modp -a $mina $maxa -o $nomfinal 
        fi 

    elif [ $d -eq 1 ] #Ici l'argument optionnel est d
      then
        if [ $w -eq 1 ] #Ici l'argument exclusif est w
        then
          ./exe -w -d $mind $maxd -o $nomfinal
        elif [ $m -eq 1 ] #Ici l'argument exclusif est m
        then 
          ./exe -m -d $mind $maxd -o $nomfinal
        elif [ $h -eq 1 ]  #Ici l'argument exclusif est h
        then 
          ./exe -h -o $nomfinal -d $mind $maxd 
        elif [ $t -eq 1 ] #Ici l'argument exclusif est t
        then 
          ./exe -t $modt -d $mind $maxd -o $nomfinal
        elif [ $p -eq 1 ] #Ici l'argument exclusif est p
        then 
          ./exe -p $modp -d $mind $maxd -o $nomfinal
        fi

    elif [ $g -eq 1 ] #Ici l'argument optionnel est g
     then
        if [ $w -eq 1 ] #Ici l'argument exclusif est w
        then
          ./exe -w -g $ming $maxg -o $nomfinal
        elif [ $m -eq 1 ]  #Ici l'argument exclusif est m
        then 
          ./exe -m -g $ming $maxg -o $nomfinal
        elif [ $h -eq 1 ]   #Ici l'argument exclusif est h
        then 
          ./exe -h -g $ming $maxg -o $nomfinal
        elif [ $t -eq 1 ] #Ici l'argument exclusif est t
        then 
          ./exe -t $modt -g $ming $maxg -o $nomfinal
        elif [ $p -eq 1 ] #Ici l'argument exclusif est p
        then 
          ./exe -p $modp -g $ming $maxg -o $nomfinal
        fi
    fi
  elif [ $resopt -eq 2 ]  #Ici on va prendre en cas tous les cas où il y a deux arguments optionnels
    then 
      if [ $a -eq 1 ] && [ $g -eq 1 ] #Ici c'est le cas ou a et g sont présents
        then
          if [ $w -eq 1 ] #Ici l'argument exclusif est w
          then
            ./exe -w -a $mina $maxa -g $ming $maxg -o $nomfinal
          elif [ $m -eq 1 ]  #Ici l'argument exclusif est m
          then 
            ./exe -m -a $mina $maxa -g $ming $maxg -o $nomfinal
          elif [ $h -eq 1 ]   #Ici l'argument exclusif est h
          then 
            ./exe -h -o $nomfinal -a $mina $maxa -g $ming $maxg
          elif [ $t -eq 1 ] #Ici l'argument exclusif est t
          then 
            ./exe -t $modt -a $mina $maxa -g $ming $maxg -o $nomfinal
          elif [ $p -eq 1 ] #Ici l'argument exclusif est p
          then 
            ./exe -p $modp -a $mina $maxa -g $ming $maxg -o $nomfinal
          fi
      elif [ $a -eq 1 ] && [ $d -eq 1 ] #Ici c'est le cas ou a et d sont présents
        then
          if [ $w -eq 1 ]
          then
            ./exe -w -a $mina $maxa -d $mind $maxd  -o $nomfinal
          elif [ $m -eq 1 ] #Ici l'argument exclusif est m
          then 
            ./exe -m -a $mina $maxa -d $mind $maxd -o $nomfinal
          elif [ $h -eq 1 ]  #Ici l'argument exclusif est h
          then 
            ./exe -h -a $mina $maxa -d $mind $maxd -o $nomfinal
          elif [ $t -eq 1 ] #Ici l'argument exclusif est t
          then 
            ./exe -t $modt -a $mina $maxa -d $mind $maxd -o $nomfinal
          elif [ $p -eq 1 ] #Ici l'argument exclusif est p
          then 
            ./exe -p $modp -a $mina $maxa -d $mind $maxd -o $nomfinal
          fi
      elif [ $g -eq 1 ] && [ $d -eq 1 ] #Ici c'est le cas ou g et d sont présents
        then
          if [ $w -eq 1 ] #Ici l'argument optionnel est w
          then
            ./exe -w -g $ming $maxg -d $mind $maxd -o $nomfinal
          elif [ $m -eq 1 ] #Ici l'argument exclusif est m
          then 
            ./exe -m -g $ming $maxg -d $mind $maxd -o $nomfinal
          elif [ $h -eq 1 ]  #Ici l'argument exclusif est h
          then 
            ./exe -h -g $ming $maxg -d $mind $maxd -o $nomfinal
          elif [ $t -eq 1 ] #Ici l'argument exclusif est t
          then 
            ./exe -t $modt -g $ming $maxg -d $mind $maxd -o $nomfinal
          elif [ $p -eq 1 ] #Ici l'argument exclusif est p
          then 
            ./exe -p $modp -g $ming $maxg -d $mind $maxd -o $nomfinal
          fi
      fi
  elif [ $resopt -eq 3 ] #Ici c'est le cas où les 3 optionnels sont demandés
    then 
      if [ $w -eq 1 ] #Ici l'argument optionnel est w
      then
        ./exe -w -a $mina $maxa -g $ming $maxg -d $mind $maxd -o $nomfinal
      elif [ $m -eq 1 ] #Ici l'argument exclusif est m
      then 
        ./exe -m -a $mina $maxa -g $ming $maxg -d $mind $maxd -o $nomfinal
      elif [ $h -eq 1 ]  #Ici l'argument exclusif est h
      then 
        ./exe -h -a $mina $maxa -g $ming $maxg -d $mind $maxd -o $nomfinal
      elif [ $t -eq 1 ] #Ici l'argument exclusif est t
      then 
        ./exe -t $modt -a $mina $maxa -g $ming $maxg -d $mind $maxd -o $nomfinal
      elif [ $p -eq 1 ] #Ici l'argument exclusif est p
      then 
        ./exe -p $modp -a $mina $maxa -g $ming $maxg -d $mind $maxd -o $nomfinal
      fi
  fi
fi



if [ $m -eq 1 ] # Cas ou il faut faire le graph de Humidité
then
 gnuplot -e "
    set terminal png size 600,600;    
    set title 'Graph Humidite';      
    set datafile separator ';';       
    set output 'GrapheHUMIDITE.png';  
    set xlabel 'Longitude';           
    set ylabel 'Latitude';           
    set palette rgb 33,13,10;         
    set dgrid3d;                      
    set dgrid3d 100,100,10;
    splot '$nomfinal' using 4:3:2 with pm3d;" 
xdg-open GrapheHUMIDITE.png
fi

if [ $modt -eq 1 ]
then 
  gnuplot -e "
    set terminal png size 600,600;  
    set title 'Graph temp1';        
    set datafile separator ';';    
    set output 'Graphtemp1.png';    
    set xlabel 'Identifiant de la station';  
    set ylabel 'Valeur';            
    plot '$nomfinal' using 1:4:2:3 with yerrorbars;" 
xdg-open Graphtemp1.png
fi

if [ $modp -eq 1 ]
then 
  gnuplot -e "
    set terminal png size 600,600; 
    set title 'Graph pre1';        
    set datafile separator ';';    
    set output 'pre1.png';          
    set xlabel 'Identifiant de la station'; 
    set ylabel 'Valeur';            
    set yrange  [ 80000 : 120000 ]; 
    plot '$nomfinal' using 1:4:2:3 with yerrorbars;"
xdg-open pre1.png
fi

if [ $h -eq 1 ]
then 
  gnuplot -e "
    set terminal png size 600,600;    
    set title 'Hauteur';              
    set datafile separator ';';       
    set output 'Hauteur.png';        
    set xlabel 'Longitude';
    set ylabel 'Latitude';           
    set palette rgb 33,13,10;         
    set dgrid3d;
    set dgrid3d 50,50,10;
    splot '$nomfinal' using 4:3:2 with pm3d;"
xdg-open Hauteur.png
fi  

if [ $w -eq 1 ]
then 
  gnuplot -e "
    set terminal png size 600,600;    
    set title 'Vent';                 
    set datafile separator ';';       
    set output 'vent.png';            
    set xlabel 'Longitude';           
    set ylabel 'Latitude';         
    plot '$nomfinal' using 5:4:6:7 with vectors;"
xdg-open vent.png
fi

if [ $modt -eq 2 ]
then 
  gnuplot -e "
    set terminal png;
    set title 'Temp2';
    set datafile separator ';'; 
    set output 'Temp2.png';
    set xdata time; 
    set timefmt '%Y-%m-%d-%H'; 
    set format x '%d\J-%H\h'; 
    set style data points;
    set xlabel 'Date'; 
    set ylabel 'Temperature'; 
    plot '$nomfinal' using 1:2 with lines;"
xdg-open Temp2.png
fi

if [ $modp -eq 2 ]
then 
  gnuplot -e "
    set terminal png;    
    set title 'Press2';  
    set datafile separator ';'; 
    set output 'Press2.png';  
    set xdata time;
    set timefmt '%Y-%m-%d-%H';
    set format x '%d\J-%H\h';
    set style data points;
    set xlabel 'Date'; 
    set ylabel 'Pression'; 
    plot '$nomfinal' using 1:2 with lines;"
xdg-open Press2.png
fi

heurefin=`date +%s.%N | cut -b-12`  #On récupère l'heure de la fin
resultat=$(echo "$heurefin-$heuredebut" | bc -l) #Calcul pour trouver le temps d'exécution
echo "Le temps d'éxécution du programme est de :" $resultat

make clean
rm exe


