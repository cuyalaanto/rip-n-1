exe: fonction.o main.o
	@echo "compilation de l'executable"
	gcc -Wall fonction.o main.o -o exe -lm -ggdb3

main.o: main.c fonction.h
	@echo "compilations du main"
	gcc -c main.c -o main.o -Wall -lm -ggdb3

fonction.o: fonction.c fonction.h
	@echo "compilations de fonctions"
	gcc -c fonction.c -o fonction.o -Wall -lm -ggdb3

clean:
	rm -f *.o
