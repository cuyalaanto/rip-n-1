#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>
#include <unistd.h>
#include "fonction.h"
#define N 1000
int he=0;
int t=0;    /* initialisation de la variable t à 0, t représentant la température */
int p=0;    /* initialisation de la variable p à 0, p représentant la pression */
int w=0;    /* initialisation de la variable w à 0, w représentant le vent */
int h=0;    /* initialisation de la variable h à 0, h représentant hauteur */
int m=0;    /* initialisation de la variable m à 0, m représentant l'humidité*/
int la=0;   /* initialisation de la variable la à 0, la représentant la latitude */
int lo=0;   /* initialisation de la variable lo à 0, lo représentant la longitude */
int d=0;    /* initialisation de la variable d à 0, d représentant la date */
int compt=1;    /* initialisation de la variable compt à 1, compt représentant le compteur */
int g=0;    /* initialisation de la variable g à 0, g représentant longitude */
int a=0;    /* initialisation de la variable a à 0, a représentant l'altitude */
int col=0;    /* initialisation de la variable col à 0, col représentant le numéro de colonne*/
int o=0;    /* initialisation de la variable o à 0 représentant le fichier de sortie */
char* fo="meteo.csv";   /* initialisation de la variable fo à meteo.csv au cas ou il n'y aurait pas fichier de sortie prédéfinie */
float lonmin=0; /* initialisation de la variable lonmin à 0, lonmin représentant la longitude minimale */
float lonmax=0; /* initialisation de la variable lonmax à 0, lonmax représentant la longitude maximale */
float lamin=0; /* initialisation de la variable lamin à 0, lamin représentant la latitude minimale */
float lamax=0; /* initialisation de la variable lamax à 0, lamax représentant la latitude maximale */
int amin=0; /* initialisation de la variable amin à 0, amin représentant l'année minimale */
int amax=0; /* initialisation de la variable amax à 0, amax représentant l'année maximale */
int mmin=0; /* initialisation de la variable mmin à 0, mmin représentant le mois minimum */
int mmax=0; /* initialisation de la variable mmax à 0, mmax représentant le mois maximum */
int jmin=0; /* initialisation de la variable jmin à 0, jmin représentant le jour minimum */
int jmax=0; /* initialisation de la variable jmax à 0, jmax représentant le jour maximum */
int main(int argc, char** argv) {
  printf("Début du projet\n");
  int opt;
  while ((opt = getopt(argc, argv, "t:p:whHmg:a:d:o:"))!=-1) {
    switch (opt) {
      case 't':   /* L'action température souhaite être utilisée */
        if (atoi(optarg)==1) {    /* On vérifie si le mode 1 de température souhaite être utilisé */
          t=1;
          compt=compt+1;    /* Le compteur prend plus 1 */
          col=10;
        }
        else{
          if (atoi(optarg)==2) {    /* On vérifie si le mode 2 de température souhaite être utilisé */
            t=2;
            compt=compt+1;
            col=10;
          }
          else{
            if (atoi(optarg)==3) {    /* On vérifie si le mode 3 de température souhaite être utilisé */
              t=3;
              compt=compt+1;
              col=10;
            }
            else{
              printf("Le mode n'est pas bien définie\n");
            }
          }
        }
      break;
      case 'p':
        p=1;
        if (atoi(optarg)==1) {    /* On vérifie si le mode 1 de pression souhaite être utilisé */
          p=1;
          compt=compt+1;
          col=2;
        }
        else{
          if (atoi(optarg)==2) {    /* On vérifie si le mode 2 de pression souhaite être utilisé */
            p=2;
            compt=compt+1;
            col=2;
          }
          else{
            if (atoi(optarg)==3) {    /* On vérifie si le mode 3 de pression souhaite être utilisé */
              p=3;
              compt=compt+1;
              col=2;
            }
            else{
              printf("Le mode n'est pas bien définie\n");
          }
        }
      }
      break;
      case 'w': /* L'action vent souhaite être utilisée donc la variable action correspondante prend la valeur de 1 */
        w=1;
        col=4;
      break;
      case 'h': /* L'action hauteur souhaite être utilisée donc la variable action correspondante prend la valeur de 1 */
        h=1;
      break;
      case 'm': /* L'action humidité souhaite être utilisé donc la variable action correspondante prend la valeur de 1 */
        m=1;
        col=5;
      break;
      case 'g':   /* L'action longitude souhaite être utilisé donc la variable action correspondante prend la valeur de 1 */
        g=1;
        lonmin=atof(argv[compt+1]);
        lonmax=atof(argv[compt+2]);
        compt=compt+2;
      break;
      case 'a':   /* L'action altitude souhaite être utilisé donc la variable action correspondante prend la valeur de 1 */
        a=1;
        lamin=atof(argv[compt+1]);
        lamax=atof(argv[compt+2]);
        compt=compt+2;
      break;
      case 'd':   /* L'action date souhaite être utilisé donc la variable action correspondante prend la valeur de 1 */
        d=1;
        sscanf(argv[compt+1],"%d-%d-%d",&amin,&mmin,&jmin);
        sscanf(argv[compt+2],"%d-%d-%d",&amax,&mmax,&jmax);
        compt=compt+2;
      break;
      case 'o':   /* L'action fichier de sortie souhaite être utilisé donc la variable action correspondante prend la valeur de 1 */
        o=1;
        fo=argv[compt+1];  //on remplace le nom du fichier de sortie par le nouveau
        compt=compt+1;
      break;
      case 'H':
        he=1;
      break;
      default:
      break;
    }
    compt=compt+1;
  }

arbre a1=NULL;
a1=lire("mesdonnees.csv",col,a1,g,a,d,lonmin,lonmax,lamin,lamax,amin,amax,mmin,mmax,jmin,jmax);


if (t==1) {   /* si le mode de température choisit est le mode 1 alors on rentre dans le if */
  arbtemp at=NULL;
  at=temp1(a1,at);
  ecrireT(at,fo);
  freenoeud(a1);
}
if (t==2) {   /* si le mode de température choisit est le mode 2 alors on rentre dans le if */
  arbreannee ann=NULL;
  ann=pres2(a1,ann);
  ecrireT2(ann,fo);
  free(a1);
}
if (t==3) {   /* si le mode de température choisit est le mode 3 alors on rentre dans le if */
  printf("fonction en cours de dev\n");
}
if (p==1) {   /* si le mode de pression choisit est le mode 1 alors on rentre dans le if */
  arbtemp a2=NULL;
  a2=pres1(a1,a2);
  ecrireP(a2,fo);
  freenoeud(a1);
}
if (p==2) {   /* si le mode de pression choisit est le mode 2 alors on rentre dans le if */
  arbreannee ann=NULL;
  ann=pres2(a1,ann);
  ecrireT2(ann,fo);
  freenoeud(a1);
}
if (p==3) {   /* si le mode de pression choisit est le mode 3 alors on rentre dans le if */
  printf("fonction en cours de dev\n");
}
if (h==1) {   /* si h=1, alors l'utilisateur veut la hauteur et on rentre donc dans le if  */
  arbreh ahaut=NULL;
  ahaut=hauteur(a1,ahaut);
  ecrireh(ahaut,fo);
  free(a1);
}
if (m==1) {   /* si m=1, alors l'utilisateur veut l'humidité' et on rentre donc dans le if  */
  arbreH ah=NULL;
  ah=humidite(a1,ah);
  ecrireH(ah,fo);
  freenoeud(a1);
}
if (w==1)   /* si m=1, alors l'utilisateur veut le 'vent' et on rentre donc dans le if  */
{
  arbreV aV=NULL;
  aV=souffle(a1,aV);
  ecrireV(aV,fo);
}
if (he==1)
{
  help();
}



  return 0;
}
