#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>
#include <unistd.h>
#include "fonction.h"
#define N 1000

arbre insertionArbre (int ID,int alt,int code,float latitude,float longitude,float donnee,arbre a,int annee,int mois,int jour,int heure,float dir){
  if (a==NULL) {
    a=creerNoeud(ID,alt,code,latitude,longitude,donnee,annee,mois,jour,heure,dir);
  }
  else{
    if (ID<a->IDsta->ID) {    //On trie l'arbre à partir des ID de station
      a->filsG=insertionArbre(ID,alt,code,latitude,longitude,donnee,a->filsG,annee,mois,jour,heure,dir);
    }
    else{
      a->filsD=insertionArbre(ID,alt,code,latitude,longitude,donnee,a->filsD,annee,mois,jour,heure,dir);
    }
  }
  return a;
}

arbreH insertionAH (int ID,float donnee,float lat,float lon,arbreH a){
  if (a==NULL) {
    a=creerNoeudBis(ID,donnee,lat,lon);
  }
  else{
    if (donnee<=a->humid->donnee) {   //On trie l'arbre à partir des données humidité
      a->filsG=insertionAH(ID,donnee,lat,lon,a->filsG);
    }
    else{
      if (donnee>a->humid->donnee)
      {
        a->filsD=insertionAH(ID,donnee,lat,lon,a->filsD);
      }else{
        return a;
      }
    }
  }
  return a;
}


arbreh insertionAh (int ID,int donnee,float lat,float lon,arbreh a){
  if (a==NULL) {
    a=creerNoeudBish(ID,donnee,lat,lon);
  }
  else{
    if (donnee<=a->altitude->donnee) {    //On trie l'arbre à partir des données d'altitude
      a->filsG=insertionAh(ID,donnee,lat,lon,a->filsG);
    }
    else{
      if (donnee>a->altitude->donnee)
      {
        a->filsD=insertionAh(ID,donnee,lat,lon,a->filsD);
      }else{
        return a;
      }
    }
  }
  return a;
}

arbreh creerNoeudBish(int ID,int donnee,float lat,float lon){   //On crée un noeud d'arbre hauteur
  arbreh m=NULL;
  m=malloc(sizeof(noeudH)*1);
  m->altitude = creerpointBish(ID,donnee,lat,lon);
  m->filsG=NULL;
  m->filsD=NULL;
  return m;
}

altitude creerpointBish(int ID,int donnee,float lat,float lon){   //On crée la structure point de hauteur
  altitude p=NULL;
  p=malloc(sizeof(pointh)*1);
  p->donnee=donnee;
  p->ID=ID;
  p->lat=lat;
  p->lon=lon;
  return p;
}


arbtemp insertionAT (int ID,float min,float max,float moy,float lat,float lon,arbtemp a){   
  if ( a==NULL){
    a=creerNoeudT(ID,min,max,moy,lat,lon);
  }
  else{
    if(ID<=a->temp->ID){    //On trie l'arbre à partir des ID de station
      a->filsG=insertionAT(ID,min,max,moy,lat,lon,a->filsG);
    }
    else{
      if (ID>a->temp->ID){
        a->filsD=insertionAT(ID,min,max,moy,lat,lon,a->filsD);
      }
      else{
        return a;
      }
    }
  }
  return a;
}


arbreannee insertionAann(int annee,int mois,int jour,int heure,float val,arbreannee a){
  if (a==NULL){
    a=creerNoeudAann(annee,mois,jour,heure,val);
  }
  else{
    if(annee<=a->annee){    //On trie l'arbre à partir des années
      a->filsG=insertionAann(annee,mois,jour,heure,val,a->filsG);
    }
    else{
      if (annee>=a->annee){
        a->filsD=insertionAann(annee,mois,jour,heure,val,a->filsD);
      }
      else{
        return a;
      }
    }
  }
  return a;
}

arbremois insertionAmois(int mois,int jour,int heure,float val,arbremois a){
  if (a==NULL){
    a=creerNoeudAmois(mois,jour,heure,val);
  }
  else{
    if(mois<=a->mois){    //On trie l'arbre à partir des mois
      a->filsG=insertionAmois(mois,jour,heure,val,a->filsG);
    }
    else{
      if (mois>=a->mois){
        a->filsD=insertionAmois(mois,jour,heure,val,a->filsD);
      }
      else{
        return a;
      }
    }
  }
  return a;
}

arbrejour insertionAjour(int jour,int heure,float val,arbrejour a){
  if (a==NULL){
    a=creerNoeudAjour(jour,heure,val);
  }
  else{
    if(jour<=a->jour){    //On trie l'arbre à partir des jours
      a->filsG=insertionAjour(jour,heure,val,a->filsG);
    }
    else{
      if (jour>=a->jour){
        a->filsD=insertionAjour(jour,heure,val,a->filsD);
      }
      else{
        return a;
      }
    }
  }
  return a;
  
}

arbreheure insertionAheure(int heure,float val,arbreheure a){
  if (a==NULL){
    a=creerNoeudAheure(heure,val);
  }
  else{
    if(heure<=a->heure){    //On trie l'arbre à partir des heures
      a->filsG=insertionAheure(heure,val,a->filsG);
    }
    else{
      if (heure>=a->heure){
        a->filsD=insertionAheure(heure,val,a->filsD);
      }
      else{
        return a;
      }
    }
  }
  return a;
}


arbreannee creerNoeudAann(int annee,int mois,int jour,int heure,float val){   //On crée un noeud d'arbre année
  arbreannee m=NULL;
  m=malloc(sizeof(noeudannee)*1);
  m->annee=annee;
  m->arbm=NULL;
  m->arbm=insertionAmois(mois,jour,heure,val,m->arbm);  //On transfère les autres valeurs dans un arbre mois qui est dans l'arbre année
  m->filsG=NULL;
  m->filsD=NULL;
  return m;
}

arbremois creerNoeudAmois(int mois,int jour,int heure,float val){   //On crée un noeud d'arbre mois
  arbremois m=NULL;
  m=malloc(sizeof(noeudmois)*1);
  m->mois=mois;
  m->arbj=NULL;
  m->arbj=insertionAjour(jour,heure,val,m->arbj);   //On transfère les autres valeurs dans un arbre jour qui est dans l'arbre mois
  m->filsG=NULL;
  m->filsD=NULL;
  return m;
}

arbrejour creerNoeudAjour(int jour,int heure,float val){    //On crée un noeud d'arbre jour
  arbrejour m=NULL;
  m=malloc(sizeof(noeudjour)*1);
  m->jour=jour;
  m->arbh=NULL;
  m->arbh=insertionAheure(heure,val,m->arbh);   //On transfère les autres valeurs dans un arbre heure qui est dans l'arbre jour
  m->filsG=NULL;
  m->filsD=NULL;
  return m;
}

arbreheure creerNoeudAheure(int heure,float val){   //On crée un arbre heure
  arbreheure m=NULL;
  m=malloc(sizeof(noeudheure)*1);
  m->heure=heure;
  m->liste=NULL;
  m->liste=insertionlisted(val,m->liste);   //On transfère la valeur étudié dans une liste de valeur
  m->filsG=NULL;
  m->filsD=NULL;
  return m;
}

listed creerMaillonD(float val){    //On crée un maillon pour ajouter notre valeur dans la liste des arbres des dates
  maillond* m=NULL;
  m=malloc(sizeof(maillond)*1);
  m->val=val;
  return m;
}

listed insertionlisted(int val,listed l){   //On insère notre valeur dans la liste
  maillond* m;
  m=creerMaillonD(val);   //On crée un maillon pour insérer dans la liste
  m->suivant=l;
  return m;
}


arbreH creerNoeudBis(int ID,float donnee,float lat,float lon){    //On crée un noeud d'arbre humidité
  arbreH m=NULL;
  m=malloc(sizeof(noeudH)*1);
  m->humid = creerpointBis(ID,donnee,lat,lon);
  m->filsG=NULL;
  m->filsD=NULL;
  return m;
}

humid creerpointBis(int ID,float donnee,float lat,float lon){   //On crée un point d'arbre humidité
  humid p=NULL;
  p=malloc(sizeof(pointH)*1);
  p->donnee=donnee;
  p->ID=ID;
  p->lat=lat;
  p->lon=lon;
  return p;
}

liste insertionliste(float val,liste l,int annee,int mois,int jour,int heure,float dir){    //On insère notre valeur dans la liste
  maillon* m;
  m=creerMaillon(val,annee,mois,jour,heure,dir);    //On crée un maillon pour pouvoir insérer notre valeur
  m->suivant=l;
  return m;
}

arbremois verifann(int annee,arbreannee a,arbremois compt){   
  if (a!=NULL)
  {
    if (annee==a->annee)    //Si la branche année existe déjà on renvoie l'arbre mois correspondant
    {
      compt=a->arbm;
    }else{
      compt=verifann(annee,a->filsD,compt);
      compt=verifann(annee,a->filsG,compt);
    }
    
  }
  return compt;
}

arbrejour verifmois(int mois,arbremois a,arbrejour compt){
  if (a!=NULL)
  {
    if (mois==a->mois)
    {
      compt=a->arbj;    //Si la branche mois existe déjà on renvoie l'arbre jour correspondant
    }else{
      compt=verifmois(mois,a->filsD,compt);
      compt=verifmois(mois,a->filsG,compt);
    }
    
  }
  return compt;
}

arbreheure verifjour(int jour,arbrejour a,arbreheure compt){
  if (a!=NULL)
  {
    if (jour==a->jour)
    {
      compt=a->arbh;    //Si la branche jour existe déjà on renvoie l'arbre heure correspondant
    }else{
      compt=verifjour(jour,a->filsD,compt);
      compt=verifjour(jour,a->filsG,compt);
    }
    
  }
  return compt;
}

arbreheure verifheure(int heure,arbreheure a,arbreheure compt){
  if (a!=NULL)
  {
    if (heure==a->heure)
    {
      compt=a;    //Si la branche heure existe déjà on renvoie la branche corerespondante
    }else{
      compt=verifheure(heure,a->filsD,compt);
      compt=verifheure(heure,a->filsG,compt);
    }
    
  }
  return compt;
}




IDstation verifID(int ID,arbre a,IDstation compt){
  if (a!=NULL)
  {
    if (ID==a->IDsta->ID)
    {
      compt=a->IDsta;   //Si la branche D'ID de station existe déjà alors on renvoie la branche de station correspondante
    }else
    {
      compt=verifID(ID,a->filsD,compt);
      compt=verifID(ID,a->filsG,compt);
    }

  }
  return compt;
}



noeud* creerNoeud(int ID,int alt,int code,float latitude,float longitude,float donnee,int annee,int mois,int jour,int heure,float dir){
  noeud* m;
  m = malloc(sizeof(noeud)*1);    //crée un noeud pour l'ajouter dans l'arbre
  m->IDsta=creerpoint(ID,alt,code,latitude,longitude,donnee,annee,mois,jour,heure,dir);
  return m;
}

maillon* creerMaillon(float donnee,int annee,int mois,int jour,int heure,float dir){
  maillon* m=NULL;
  m=malloc(sizeof(maillon)*1);    //crée un maillon pour ajouter les valeurs dans la liste
  m->val=donnee;
  m->annee=annee;
  m->mois=mois;
  m->jour=jour;
  m->heure=heure;
  m->dir=dir;
  return m;
}

point* creerpoint(int ID,int alt,int code,float latitude,float longitude,float donnee,int annee,int mois,int jour,int heure,float dir){
  point* p=NULL;
  p=malloc(sizeof(point)*1);    //on crée un point pour enregistrer nos valeurs dans la structure
  p->ID=ID;
  p->alt=alt;
  p->code=code;
  p->latitude=latitude;
  p->longitude=longitude;
  p->station=insertionliste(donnee,p->station,annee,mois,jour,heure,dir);
  return p;
}

temp creerpointTemp(int ID,float min,float max,float moy,float lat,float lon){
  temp t=NULL;
  t=malloc(sizeof(pointT)*1);   //On crée un point pour enregistrer nos valeurs dans une structure température
  t->ID=ID;
  t->min=min;
  t->max=max;
  t->moy=moy;
  t->lat=lat;
  t->lon=lon;
  return t;
}


arbtemp creerNoeudT(int ID,float min,float max, float moy,float lat,float lon){
  arbtemp art=NULL;
  art=malloc(sizeof(noeudT)*1);   //On crée un noeud pour ajouter nos valeurs dans l'arbre température
  art->temp = creerpointTemp(ID,min,max,moy,lat,lon);
  art->filsG=NULL;
  art->filsD=NULL;
  return art;
}


arbre lire(char* filename, int column, arbre a,int lon,int la,int da,float lonmin,float lonmax,float lamin,float lamax,int amin,int amax,int mmin,int mmax,int jmin,int jmax) {
    // Ouvrez le fichier en mode lecture
  FILE* fichier = fopen(filename, "r");
  if (fichier == NULL) {
    fprintf(stderr, "Erreur d'ouverture du fichier '%s'\n", filename);
    exit(EXIT_FAILURE);
  }

  // Lisez chaque ligne du fichier
  char line[N];
  fgets(line, N, fichier);
  while (fgets(line, N, fichier)) {
    // Faites une copie de la ligne avant de l'utiliser avec strsep
    char *line_copy = strdup(line);

    // Séparez la ligne en différentes cellules en utilisant la virgule comme séparateur
    char *cell=NULL;
    //cell=malloc(sizeof(char)*100);
    int i = 0;    //Notre compteur de colonne 
    int ID = 0;   //Variable ID
    float val=0;    //Variable valeur
    int alt=0;    //Variable altitude
    int code=0;   //Variable code postal (inutile finalement)
    float latitude=0;   //Variable latitude
    float longitude=0;    //Variable longitude
    IDstation compt=NULL;   //Variable station
    int heure=0;    //Variable heure
    int jour=0;   //Variable jour
    int mois=0;   //Variable mois
    int annee=0;    //Variable annee
    float dir=0;    //Variable direction
    while ((cell = strsep(&line_copy, ";")) != NULL) {
      if (i == 0) {   //On rentre dans le if pour la première boucle du while, c'est à dire pour la première colone
        ID=atoi(cell);    //On enregistre notre cellule dans la variable ID
      }

      if (i==column)    //On rentre dans le if pour la collonne que l'on a inscrite en entrée de la fonction
      {
        val=atof(cell);   //On enregistre notre cellule dans la variable val (valeur)
      }
      if (i==3) {
        dir=atof(cell);   //On enregistre notre cellule dans la variable dir (direction)
      }
      if (i==9)
      {
        sscanf(cell, "%f,%f", &latitude, &longitude);   //On enregistre les valeurs luent dans les variable longitude et latitude
      }
      if (i==1) {
        sscanf(cell, "%d-%d-%dT%d", &annee, &mois, &jour, &heure);    //On enregistre les valeurs luent dans les variable annee, mois, jour, heure
      }
      if (i==13) {
        alt=atoi(cell);   ////On enregistre les valeurs de la cellule dans une variable alt (altitude)
      }
      i++;
    }
    // Maintenant on regarde si il y a des tirets optionel
    if ((la==0)&&(da==0)&&(lon==0))   //Si il n'y a aucun tirets optionel
    {//Insertion classique
      compt=verifID(ID,a,NULL);   //On regarde si la branche de station existe déjà
      if (compt==NULL)
      {
        //Si il n'y a pas de branche déjà existante alors on insère une nouvelle branche
        a=insertionArbre(ID,alt,code,latitude,longitude,val,a,annee,mois,jour,heure,dir);
      }else{
        //Si il y en a une, alors on insère nos valeurs directement dans celle-ci
        compt->station=insertionliste(val,compt->station,annee,mois,jour,heure,dir);
      }
    }
    if ((la==1)&&(da==0)&&(lon==0))   //Si le mode latitude est demandé
    {
      if ((lamin<=latitude)&&(lamax>=latitude))   //On regarde si la latitude et la longitude demander est corecte au valeurs lue
      {//Insertion classique
        compt=verifID(ID,a,NULL);
      if (compt==NULL)
      {

        a=insertionArbre(ID,alt,code,latitude,longitude,val,a,annee,mois,jour,heure,dir);
      }else{

        compt->station=insertionliste(val,compt->station,annee,mois,jour,heure,dir);
      }
      }
      
    }
    if ((la==0)&&(da==1)&&(lon==0)) //Si le mode date est demandé
    {
      if ((amax>annee)&&(amin<annee))   //On regarde si l'année souhaité est corecte avec les valeurs lues
        {//Insertion classique
          compt=verifID(ID,a,NULL);
      if (compt==NULL)
      {

        a=insertionArbre(ID,alt,code,latitude,longitude,val,a,annee,mois,jour,heure,dir);
      }else{

        compt->station=insertionliste(val,compt->station,annee,mois,jour,heure,dir);
        //afficherliste(compt->station);
      }
        }else{
          if ((annee=amax)||(annee=amin)) //Si l'année est égale à l'une des bornes d'année
          {
            if ((mmin<mois)&&(mmax>mois))   //On regarde si le mois souhaité est corecte avec les valeurs lues
            { //Insertion classique
              compt=verifID(ID,a,NULL);
      if (compt==NULL)
      {

        a=insertionArbre(ID,alt,code,latitude,longitude,val,a,annee,mois,jour,heure,dir);
      }else{

        compt->station=insertionliste(val,compt->station,annee,mois,jour,heure,dir);
      }
            }else{
              if (((mmin==mois)||(mmax==mois))&&((jmin<=jour)&&(jmax>=jour)))   //Si le mois est égale à l'une des bornes de mois et si le jour souhaité est corecte avec les valeurs lues
              {//Insertion classique
                compt=verifID(ID,a,NULL);
      if (compt==NULL)
      {

        a=insertionArbre(ID,alt,code,latitude,longitude,val,a,annee,mois,jour,heure,dir);
      }else{

        compt->station=insertionliste(val,compt->station,annee,mois,jour,heure,dir);
      }
              }
              
            }
            
          }
          
        }
    }
    if ((la==0)&&(da==0)&&(lon==1))// Si le mode longitude est demandé
    {
      if ((lonmin<=longitude)&&(lonmax>=longitude))
      {
        compt=verifID(ID,a,NULL);
      if (compt==NULL)
      {

        a=insertionArbre(ID,alt,code,latitude,longitude,val,a,annee,mois,jour,heure,dir);
      }else{

        compt->station=insertionliste(val,compt->station,annee,mois,jour,heure,dir);
        //afficherliste(compt->station);
      }
      }
      
    }
    if ((la==1)&&(da==1)&&(lon==0))   //Si le mode latitude et date est demandé
    {
      if ((lamin<=latitude)&&(lamax>=latitude))
      {
        if ((amax>annee)&&(amin<annee))
        {
          compt=verifID(ID,a,NULL);
      if (compt==NULL)
      {

        a=insertionArbre(ID,alt,code,latitude,longitude,val,a,annee,mois,jour,heure,dir);
      }else{

        compt->station=insertionliste(val,compt->station,annee,mois,jour,heure,dir);
        //afficherliste(compt->station);
      }
        }else{
          if ((annee=amax)||(annee=amin))
          {
            if ((mmin<mois)&&(mmax>mois))
            {
              compt=verifID(ID,a,NULL);
      if (compt==NULL)
      {

        a=insertionArbre(ID,alt,code,latitude,longitude,val,a,annee,mois,jour,heure,dir);
      }else{

        compt->station=insertionliste(val,compt->station,annee,mois,jour,heure,dir);
        //afficherliste(compt->station);
      }
            }else{
              if (((mmin==mois)||(mmax==mois))&&((jmin<=jour)&&(jmax>=jour)))
              {
                compt=verifID(ID,a,NULL);
      if (compt==NULL)
      {

        a=insertionArbre(ID,alt,code,latitude,longitude,val,a,annee,mois,jour,heure,dir);
      }else{

        compt->station=insertionliste(val,compt->station,annee,mois,jour,heure,dir);
        //afficherliste(compt->station);
      }
              }
              
            }
            
          }
          
        }
        
      }
      
    }
    if ((la==0)&&(da==1)&&(lon==1))   //Si le mode date et longitude est demandé
    {
      if ((lonmin<=longitude)&&(lonmax>=longitude)){
         if ((amax>annee)&&(amin<annee))
        {
          compt=verifID(ID,a,NULL);
      if (compt==NULL)
      {

        a=insertionArbre(ID,alt,code,latitude,longitude,val,a,annee,mois,jour,heure,dir);
      }else{

        compt->station=insertionliste(val,compt->station,annee,mois,jour,heure,dir);
      }
        }else{
          if ((annee=amax)||(annee=amin))
          {
            if ((mmin<mois)&&(mmax>mois))
            {
              compt=verifID(ID,a,NULL);
      if (compt==NULL)
      {

        a=insertionArbre(ID,alt,code,latitude,longitude,val,a,annee,mois,jour,heure,dir);
      }else{

        compt->station=insertionliste(val,compt->station,annee,mois,jour,heure,dir);
      }
            }else{
              if (((mmin==mois)||(mmax==mois))&&((jmin<=jour)&&(jmax>=jour)))
              {
                compt=verifID(ID,a,NULL);
      if (compt==NULL)
      {

        a=insertionArbre(ID,alt,code,latitude,longitude,val,a,annee,mois,jour,heure,dir);
      }else{

        compt->station=insertionliste(val,compt->station,annee,mois,jour,heure,dir);
      }
              }
              
            }
            
          }
          
        }
      }
    }
    if ((la==1)&&(da==0)&&(lon==1))   //Si le mode latitude et longitude est demandé
    {
      if ((lamin<=latitude)&&(lamax>=latitude)){
        if ((lonmin<=longitude)&&(lonmax>=longitude)){
          compt=verifID(ID,a,NULL);
        if (compt==NULL)
        {

          a=insertionArbre(ID,alt,code,latitude,longitude,val,a,annee,mois,jour,heure,dir);
        }else{

          compt->station=insertionliste(val,compt->station,annee,mois,jour,heure,dir);
        }
        }
      }
    }
    if ((la==1)&&(da==1)&&(lon==1))//Si le mode latitude et longitude et date est demandé
    {
      if ((lamin<=latitude)&&(lamax>=latitude))
      {
        if ((lonmin<=longitude)&&(lonmax>=longitude))
        {
          if ((amax>annee)&&(amin<annee))
        {
          compt=verifID(ID,a,NULL);
      if (compt==NULL)
      {

        a=insertionArbre(ID,alt,code,latitude,longitude,val,a,annee,mois,jour,heure,dir);
      }else{

        compt->station=insertionliste(val,compt->station,annee,mois,jour,heure,dir);
      }
        }else{
          if ((annee=amax)||(annee=amin))
          {
            if ((mmin<mois)&&(mmax>mois))
            {
              compt=verifID(ID,a,NULL);
      if (compt==NULL)
      {

        a=insertionArbre(ID,alt,code,latitude,longitude,val,a,annee,mois,jour,heure,dir);
      }else{

        compt->station=insertionliste(val,compt->station,annee,mois,jour,heure,dir);
      }
            }else{
              if (((mmin==mois)||(mmax==mois))&&((jmin<=jour)&&(jmax>=jour)))
              {
                compt=verifID(ID,a,NULL);
      if (compt==NULL)
      {

        a=insertionArbre(ID,alt,code,latitude,longitude,val,a,annee,mois,jour,heure,dir);
      }else{

        compt->station=insertionliste(val,compt->station,annee,mois,jour,heure,dir);
      }
              }
              
            }
            
          }
          
        }
        }
        
      }
      
    }
    

    // Libérez la mémoire allouée pour la copie de la ligne
    free(line_copy);
  }

  // Fermez le fichier
  fclose(fichier);
  return a;
}


void afficherarbre(arbre a){    //Fonction utilitaire qui sert à afficher l'arbre d'ID de station
  if (a!=NULL)
  {
  float c=0;
  //printf("moyenne : %f\n",c);
  printf("ID : %d\n",a->IDsta->ID);
  //c=moyliste(a->IDsta->station);
  //printf("moy : %f\n",c);
  c=afficherliste(a->IDsta->station,c);
  //printf("compteur : %d\n",c);
  //printf("latitude : %f\n",a->IDsta->latitude);
  //printf("longitude: %f\n",a->IDsta->longitude);
  //printf("altitude : %d\n",a->IDsta->alt);
  afficherarbre(a->filsD);
  afficherarbre(a->filsG);
  }
}

int afficherliste(liste l,int c){   //Fonction utilitaire qui sert à afficher la liste des valeurs de station
  if (l!=NULL)
  {
    //printf("direction vent : %f\n",l->dir);
    printf("valeur %f\n",l->val);
    //printf("année : %d\n",l->annee);
    //printf("mois : %d\n",l->mois);
    //printf("jour : %d\n",l->jour);
    //printf("heure : %d\n",l->heure);
    c=afficherliste(l->suivant,c+1);
  }
  return c;
}

void ecrireV(arbreV a,char* nom){
  FILE* fichier;
  fichier=fopen(nom, "w");
  if (fichier != NULL){   //Si le fichier s'ouvre
    cacheecrireV(a,fichier);
  }
  fclose(fichier);
}

void cacheecrireV(arbreV a,FILE* fichier){    //Ecriture de l'arbre en mode infixe
  if (fichier != NULL) {
    if (a!=NULL){
      cacheecrireV(a->filsG,fichier);   //On parcourt l'arbre vers la gauche d'abord
      fprintf(fichier, "%d;%f;%f;%f;%f;%f;%f\n",a->vent->ID,a->vent->moyD,a->vent->moyV,a->vent->lat,a->vent->lon,a->vent->longa,a->vent->lato);    //écriture des valeurs de l'arbre
      cacheecrireV(a->filsD,fichier);   //Et on fini par la droite
    }
  }
}



arbreV souffle(arbre a1, arbreV a2){    //La fonction vent qui organise l'arbre vent
  float moyD=0;
  float moyV=0;
  int ID=0;
  float lon=0;
  float lat=0;
  float longa=0;
  float lato=0;
  float test=1;
  if (a1==NULL)
  {
    return a2;
  }
  else {
    test= sin(test);
    ID=a1->IDsta->ID;
    lat=a1->IDsta->latitude;
    lon=a1->IDsta->longitude;
    moyD=moylisteBis(a1->IDsta->station);   //On fait la moyenne de toutes les direction
    moyV=moyliste(a1->IDsta->station);    //On fait la moyenne de toutes les valeurs de vitesse de vent
    longa=moylistex(a1->IDsta->station);    //On fait le calcul du point final longitude    (pour les vecteurs)
    lato=moylisteBis(a1->IDsta->station);   //On fait le calcul du point final latitude     (pour les vecteurs)
    a2=insertionAV(ID,moyD,moyV,lat,lon,longa,lato,a2);   //On insère toutes nos valeurs dans l'arbre
    a2=souffle(a1->filsG,a2);   //On recommence pour toutes les branches de l'arbre de lecture
    a2=souffle(a1->filsD,a2);
  }
  return a2;
}

vent creerpointV(int ID,float moyD,float moyV,float lat,float lon,float longa,float lato){    //On crée un point pour ajouter nos valeurs dans la structure vent
  vent t=NULL;
  t=malloc(sizeof(pointV)*1);
  t->ID=ID;
  t->moyD=moyD;
  t->moyV=moyV;
  t->lat=lat;
  t->lon=lon;
  t->longa=longa;
  t->lato=lato;
  return t;
}

arbreV creerNoeudV(int ID,float moyD,float moyV,float lat,float lon,float longa,float lato){    //On crée un noeud pour ajouter nos valeurs dans l'arbre vent
  arbreV a=NULL;
  a=malloc(sizeof(noeudV)*1);
  a->vent = creerpointV(ID,moyD,moyV,lat,lon,longa,lato);
  a->filsG=NULL;
  a->filsD=NULL;
  return a;
}

arbreV insertionAV (int ID,float moyD,float moyV,float lat,float lon,float longa,float lato,arbreV a){    //On insère nos valeurs dans l'arbre de vent
  if ( a==NULL){
    a=creerNoeudV(ID,moyD,moyV,lat,lon,longa,lato);   //On enregistre nos valeurs à la bonne position
  }
  else{
    if(ID<=a->vent->ID){    //On se déplace à droite ou a gauche de l'arbre
      a->filsG=insertionAV(ID,moyD,moyV,lat,lon,longa,lato,a->filsG);
    }
    else{
      if (ID>a->vent->ID){
        a->filsD=insertionAV(ID,moyD,moyV,lat,lon,longa,lato,a->filsD);
      }
      else{
        return a;
      }
    }
  }
  return a;
}

float moylisteBis(liste l){   //On fait la moyenne des angles
  float c=0;
  float valtotal=0;
  float y=0;
  while (l->suivant!=NULL) {
    if (l->dir!=0) {
      y=0;
      y=l->val*sin(l->dir);
      valtotal=valtotal+y;
      c=c+1;
    }
  l=l->suivant;
  }
  float res=0;
  //printf("val : %f\n",valtotal);
  //printf("c : %f\n",c);
  if (c==0) {
    res=0;
  }else{
    res=(valtotal)/(c);
  }
  return res;
}

float moylistex(liste l){   //On fait la moyenne des angles
  float c=0;
  float valtotal=0;
  float x=0;
  while (l->suivant!=NULL) {
    if (l->val!=0) {
      x=0;
      x=l->val*cos(l->dir);
      valtotal=valtotal+x;
      c=c+1;
    }
    l=l->suivant;
  }
  float res=0;
  if (c==0) {
    res=0;
  }else{
    res=(valtotal)/(c);
  }
  return res;
}



arbreH humidite(arbre a1,arbreH a2){    //Fonction qui organise l'arbre humidité
  float max=0;
  int ID=0;
  float lon=0;
  float lat=0;
  if (a1==NULL)
  {
    return a2;
  }
  else{
    ID=a1->IDsta->ID;   //On enregistre notre ID de station
    lon=a1->IDsta->longitude;   //On enregistre les longitudes
    lat=a1->IDsta->latitude;    //On enregistre les latitudes
    max=maxliste(a1->IDsta->station,0);   //On enregistre le max de la liste
    a2=insertionAH(ID,max,lat,lon,a2);    //On insère nos valeurs dans l'arbre humidité
    a2=humidite(a1->filsG,a2);
    a2=humidite(a1->filsD,a2);
  }
  return a2;
}



arbtemp temp1(arbre a1, arbtemp a2){    //Fonction qui organise l'arbre température
  float max=0;
  float min=100;
  float moy=0;
  float lat=0;
  float lon=0;
  int ID=0;
  if (a1==NULL)
  {
    return a2;
  }
  else {
    ID=a1->IDsta->ID;   //On enregistre notre ID de station
    lat=a1->IDsta->latitude;    //On enregistre les longitudes
    lon=a1->IDsta->longitude;   //On enregistre les latitudes
    max=maxliste(a1->IDsta->station,max);   //On enregistre les valeurs maximums de la liste
    min=minliste(a1->IDsta->station,min);   //On enregistre les valeurs minimums de la liste
    moy=moyliste(a1->IDsta->station);   //On enregistre les valeurs moyennes de la liste
    a2=insertionAT(ID,min,max,moy,lat,lon,a2);    //On insère nos valeurs dans l'arbre température
    a2=temp1(a1->filsG,a2);   //On parcourt tout l'arbre des valeurs lues
    a2=temp1(a1->filsD,a2);
  }
  return a2;
}




arbtemp pres1(arbre a1, arbtemp a2){    //fonction qui organisse l'arbre de pression mode 1
  float max=0;
  float min=10000000;
  float moy=0;
  int ID=0;
  float lon=0;
  float lat=0;
  if (a1==NULL)
  {
    return a2;
  }
  else {
    ID=a1->IDsta->ID;   //On récupère les ID
    lat=a1->IDsta->latitude;//On récupère la latitude
    lon=a1->IDsta->longitude;//On récupère la longitude
    max=maxliste(a1->IDsta->station,max);//On récupère les valeurs maximums de chaque liste
    min=minliste(a1->IDsta->station,min);//On récupère les valeurs minimums de chaque liste
    moy=moyliste(a1->IDsta->station);//On récupère les valeurs moyennne de chaque liste
  
    a2=insertionAT(ID,min,max,moy,lat,lon,a2);//On insère toutes nos valeurs
    a2=pres1(a1->filsG,a2);
    a2=pres1(a1->filsD,a2);
  }
  return a2;
}



arbreannee pres2(arbre a1,arbreannee a2){   //fonction qui organisse l'arbre de pression ou température mode 2
  if (a1==NULL)
  {
    return a2;
  }else{

    a2=pres2cache(a1->IDsta->station,a2);
    a2=pres2(a1->filsG,a2);
    a2=pres2(a1->filsD,a2);
  }
  return a2;
}

arbreannee pres2cache(liste l,arbreannee a2){   //fonction qui insère insère au bon endroit de l'arbre pression ou température mode 2
  if (l==NULL)
  {
    return a2;
  }else{
    arbremois comptann=NULL;
    arbrejour comptmois=NULL;
    arbreheure comptjour=NULL;
    arbreheure comptheure=NULL;
    comptann=verifann(l->annee,a2,comptann);    //On regarde si la branche d'année correspondante existe déjà
    if (comptann==NULL)
    {
      a2=insertionAann(l->annee,l->mois,l->jour,l->heure,l->val,a2);
    }else{
      comptmois=verifmois(l->mois,comptann,comptmois);    //On regarde si la branche mois correspondante existe déjà
      if (comptmois==NULL)
      {
        comptann=insertionAmois(l->mois,l->jour,l->heure,l->val,comptann);
      }else{
        comptjour=verifjour(l->jour,comptmois,comptjour);   //On regarde si la branche jour correspondante existe déjà
        if (comptjour==NULL)
        {
          comptmois=insertionAjour(l->jour,l->heure,l->val,comptmois);
        }else{
          comptheure=verifheure(l->heure,comptjour,comptheure);   //On regarde si la branche heure correspondante existe déjà
          if (comptheure==NULL)
          {
            comptjour=insertionAheure(l->heure,l->val,comptjour);
          }else{
            comptheure->liste=insertionlisted(l->val,comptheure->liste);
          }
          
        }
        
      }
      
    }
    a2=pres2cache(l->suivant,a2); 
  }
  return a2;
}

arbreh hauteur(arbre a1,arbreh a2){//arbre qui organise l'arbre hauteur
  int ID=0;
  int alt;
  float lon=0;
  float lat=0;
  if (a1==NULL)
  {
    return a2;
  }
  else{
    ID=a1->IDsta->ID;
    alt=a1->IDsta->alt;
    lat=a1->IDsta->latitude;
    lon=a1->IDsta->longitude;
    a2=insertionAh(ID,alt,lat,lon,a2);
    a2=hauteur(a1->filsG,a2);
    a2=hauteur(a1->filsD,a2);
  }
  return a2;
}

void ecrireh(arbreh a,char* nom){
  FILE* fichier;
  fichier=fopen(nom, "w");    //Ouverture du fichier en mode écriture
  if (fichier != NULL) {
    cacheecrireh(a,fichier);
  }
  fclose(fichier);
}

void cacheecrireh(arbreh a,FILE* fichier){
  if (fichier != NULL) {
    if (a!=NULL){
    cacheecrireh(a->filsD,fichier);
    fprintf(fichier, "%d;%d;%f;%f\n",a->altitude->ID,a->altitude->donnee,a->altitude->lat,a->altitude->lon);    //On écrit nos valeurs en parcour infixe
    cacheecrireh(a->filsG,fichier);
    }
  }
}



float maxliste(liste l,float val){
  if (l==NULL)
  {
    return val;
  }
  else{
    if (l->val>val)   //On ne garde que le maximum des valeurs
    {
      val=l->val;
    }
    return maxliste(l->suivant,val);
  }

}

float minliste(liste l,float val){
  if (l==NULL)
  {
    return val;
  }
  else{
    if (l->val!=0){
      if (l->val<val)   //On ne garde que le minimum des valeurs
    {
        val=l->val;
    }
  }
    return minliste(l->suivant,val);
  }
}


void freemaillon(liste l){    //Plusieurs fonctions de libération
  if (l!=NULL)
  {
    freemaillon(l->suivant);
    free(l);
  }

}

void freepoint(IDstation l){
  freemaillon(l->station);
  free(l);
}

void freenoeud(arbre a){
  if (a!=NULL)
  {
    freenoeud(a->filsD);
    freenoeud(a->filsG);
    freepoint(a->IDsta);
    free(a);
  }

}

void freenoeudBis(arbreH a){
  if (a!=NULL)
  {
    freenoeudBis(a->filsD);
    freenoeudBis(a->filsG);
    freepointBis(a->humid);
    free(a);
  }

}

void freepointBis(humid l){
  free(l);
}

void freepointTemp(temp l){
  free(l);
}

void freenoeudTemp(arbtemp a){
  if (a!=NULL)
  {
    freenoeudTemp(a->filsD);
    freenoeudTemp(a->filsG);
    freepointTemp(a->temp);
    free(a);
  }
}


void afficherarbreBis(arbreH a){
  if (a!=NULL)
  {
    printf("ID : %d\n",a->humid->ID);
    printf("donnee : %f\n",a->humid->donnee);
    afficherarbreBis(a->filsD);
    afficherarbreBis(a->filsG);
  }

}

void afficherarbreTemp(arbtemp a){
  if (a!=NULL)
  {
    printf("ID : %d\n",a->temp->ID);
    printf("min : %f\n",a->temp->min);
    printf("min : %f\n",a->temp->max);
    printf("min : %f\n",a->temp->moy);
    afficherarbreTemp(a->filsD);
    afficherarbreTemp(a->filsG);
  }
}

void afficherarbreAnn(arbreannee a){
  if (a!=NULL)
  {
    printf("Année : %d\n",a->annee);
    afficherarbremois(a->arbm);
    afficherarbreAnn(a->filsD);
    afficherarbreAnn(a->filsG);
  }
  
}

void afficherarbremois(arbremois a){
  if (a!=NULL)
  {
    printf("  Mois : %d\n",a->mois);
    afficherarbrejour(a->arbj);
    afficherarbremois(a->filsD);
    afficherarbremois(a->filsG);
  }
  
}

void afficherarbrejour(arbrejour a){
  if (a!=NULL)
  {
    printf("    jour : %d\n",a->jour);
    afficherarbreheure(a->arbh);
    afficherarbrejour(a->filsD);
    afficherarbrejour(a->filsG);
  }
  
}

void afficherarbreheure(arbreheure a){
  if (a!=NULL)
  {
    printf("      Heure : %d\n",a->heure);
    afficherarbrelisted(a->liste);
    afficherarbreheure(a->filsD);
    afficherarbreheure(a->filsG);
  }
  
}

void afficherarbrelisted(listed a){
  if (a!=NULL)
  {
    printf("          Val : %f\n",a->val);
    afficherarbrelisted(a->suivant);
  }
  
}


float moyliste(liste l){   
  float c=0;
  float valtotal=0;
  while (l->suivant!=NULL) {
    if (l->val!=0) {
      valtotal=valtotal+l->val;
      c=c+1;
    }
    l=l->suivant;
  }
  float res=0;
  if (c==0) {
    res=0;
  }else{
    res=(valtotal)/(c);   //On calcule la moyenne
  }
  return res;
}

void ecrireH(arbreH a,char* nom){
  FILE* fichier;
  fichier=fopen(nom, "w");    //ouverture du fichier en mode écriture
  if (fichier != NULL) {
    cacheecrireH(a,fichier);
  }
  fclose(fichier);
  }

void cacheecrireH(arbreH a,FILE* fichier){
    if (fichier != NULL) {
      if (a!=NULL){
      cacheecrireH(a->filsG,fichier);
      fprintf(fichier, "%d;%f;%f;%f\n",a->humid->ID,a->humid->donnee,a->humid->lat,a->humid->lon);    //On écrit nos valeurs en parcour infixe
      cacheecrireH(a->filsD,fichier);
    }
      }
}


void ecrireT(arbtemp a,char* nom){
  FILE* fichier;
  fichier=fopen(nom, "w");     //ouverture du fichier en mode écriture
  if (fichier != NULL){
    cacheecrireT(a,fichier);
  }
  fclose(fichier);
}

void ecrireP(arbtemp a,char* nom){
  FILE* fichier;
  fichier=fopen(nom, "w");     //ouverture du fichier en mode écriture
  if (fichier != NULL){
    cacheecrireT(a,fichier);
  }
  fclose(fichier);
}

void cacheecrireT(arbtemp a,FILE* fichier){
  if (fichier != NULL) {
    if (a!=NULL){
      cacheecrireT(a->filsG,fichier);
      fprintf(fichier, "%d;%f;%f;%f,%f,%f\n",a->temp->ID,a->temp->min,a->temp->max,a->temp->moy,a->temp->lat,a->temp->lon);   //On écrit nos valeurs en parcour infixe
      cacheecrireT(a->filsD,fichier);
    }
  }
}


void ecrireT2(arbreannee a,char* nom){
  FILE* fichier;
  fichier=fopen(nom,"w");
  if (fichier !=NULL)
  {
    ecrireT2ann(a,fichier);   //On envoie l'année de la branche
  }
  fclose(fichier);
}

void ecrireT2ann(arbreannee a,FILE* fichier){   //On envoie le mois de la branche
  if (fichier!=NULL)
  {
    if (a!=NULL)
    {
      ecrireT2ann(a->filsG,fichier);
      ecrireT2mois(a->annee,a->arbm,fichier);
      ecrireT2ann(a->filsD,fichier);
    }
    
  }
  
}

void ecrireT2mois(int annee,arbremois a,FILE* fichier){
  if (fichier!=NULL)
  {
    if (a!=NULL)
    {
      ecrireT2mois(annee,a->filsG,fichier);
      ecrireT2jour(a->mois,annee,a->arbj,fichier);    //On envoie le jour de la branche
      ecrireT2mois(annee,a->filsD,fichier);
    }
    
  }
  
}

void ecrireT2jour(int mois,int annee,arbrejour a,FILE* fichier){
  if (fichier!=NULL)
  {
    if (a!=NULL)
    {
      ecrireT2jour(mois,annee,a->filsG,fichier);
      ecrireT2heure(a->jour,mois,annee,a->arbh,fichier);    //On envoie le jour de la branche
      ecrireT2jour(mois,annee,a->filsD,fichier);
    }
    
  }
  
}

void ecrireT2heure(int jour,int mois,int annee,arbreheure a,FILE* fichier){
  if (fichier!=NULL)
  {
    if (a!=NULL)
    {
      ecrireT2heure(jour,mois,annee,a->filsG,fichier);
      float moyval;
      moyval=moylisted(a->liste);   //Moyenne des valeurs pour une même date
      fprintf(fichier,"%d-%d-%d-%d;%f\n",annee,mois,jour,a->heure,moyval);    //On écrit nos valeurs
      ecrireT2heure(jour,mois,annee,a->filsD,fichier);
    }
    
  }
  
}

float moylisted(listed l){
  float c=0;
  float valtotal=0;
  while (l!=NULL) {
    if (l->val!=0) {
      valtotal=valtotal+l->val;
      c=c+1;
    }
    l=l->suivant;
  }
  float res=0;
  if (c==0) {
    res=0;
  }else{
    res=(valtotal)/(c);   //On clacule la moyenne des valeurs
  }
  return res;
}


void help(){
  printf("\n Aide détaillé du programme C \n \n");
  printf("--> ARGUMENTS : \n");
  printf("    -t <mode> : (t)empératures.\n\n");
  printf("    -p <mode> : (p)ressions atmosphériques.\n");
  printf("         Pour ces 2 options, il faut indiquer la valeur de <mode> :\n");
  printf("           ¤ 1 : produit en sortie les températures ( ou pressions )\n");
  printf("                 minimales, maximales et moyennes par station dans l'ordre\n");
  printf("                 croissant du numéro de station.\n");
  printf("           ¤ 2 : produit en sortie les températures (ou pressions)\n");
  printf("                 moyennes par date/heure, triées dans l’ordre\n");
  printf("                 chronologique. La moyenne se fait sur toutes les stations.\n");
  printf("           ¤ 3 : produit en sortie les températures (ou pressions) par\n");
  printf("                 date/heure par station. Elles seront triées d’abord par ordre\n");
  printf("                 chronologique, puis par ordre croissant de l’identifiant de la\n");
  printf("                 station.\n\n");
  printf("    -w : vent ( (w)ind ).\n");
  printf("         Produit en sortie l’orientation moyenne et la vitesse moyenne des\n");
  printf("         vents pour chaque station. Quand on parle de moyenne, il s’agira\n");
  printf("         de faire la somme pour chaque composante du vecteur, et d’en\n");
  printf("         faire la moyenne une fois tous les vecteurs traités. On aura donc\n");
  printf("         une moyenne sur l’axe X et une moyenne sur l’axe Y : les 2 résultats\n");
  printf("         fournissant le module et l’orientation moyens demandés. Les\n");
  printf("         données seront triées par identifiant croissant de la station.\n\n");
  printf("    -h : (h)auteur.\n");
  printf("         Produit en sortie la hauteur pour chaque station. Les hauteurs\n");
  printf("         seront triées par ordre décroissant.\n");
  printf("    -m : humidité ( (m)oisture ).\n");
  printf("         Produit en sortie l’humidité maximale pour chaque station. Les\n");
  printf("         valeurs d’humidités seront triées par ordre décroissant.\n\n");
  printf("    -g <min> <max> : lon(g)itude.\n");
  printf("         Permet de filtrer les données de sortie en ne gardant que les\n");
  printf("         données qui sont dans l’intervalle de longitudes [<min>..<max>]\n");
  printf("         incluses. Le format des longitudes est un nombre réel.\n\n");
  printf("    -a <min> <max> : l(a)titude.\n");
  printf("         Permet de filtrer les données de sortie en ne gardant que les\n");
  printf("         relevés qui sont dans l’intervalle de latitudes [<min>..<max>]\n");
  printf("         incluses. Le format des latitudes est un nombre réel.\n\n");
  printf("    -d <min> <max> : (d)ates.\n");
  printf("         Permet de filtrer les données de sortie en ne gardant que les\n");
  printf("         relevés qui sont dans l’intervalle de dates [<min>..<max>] incluses.\n");
  printf("         Le format des dates est une chaine de type YYYY-MM-DD (année-\n");
  printf("         mois-jour).\n\n");
  printf("    -i <nom_fichier> : (f)ichier d’entrée.\n");
  printf("         Permet de spécifier le chemin du fichier CSV d’entrée (fichier\n");
  printf("         fourni). Cette option est obligatoire.\n\n");
  printf("    -o <nom_fichier> : (f)ichier de sortie.\n");
  printf("         Permet de donner un nom au fichier de sortie contenant les\n");
  printf("         données. Si les options utilisées nécessitent plusieurs fichiers, cet\n");
  printf("         argument peut servir à définir le préfixe des fichiers de sortie.\n");
  printf("         Si ce champ n’est pas fourni, la sortie s’appellera ‘meteoxxxx.dat’\n");
  printf("         par défaut, avec xxxx un nombre sur 4 digits.\n\n");
  printf("--> Les options -g, -a et -d sont optionnelles. Elles ne servent qu’à restreindre le\n");
  printf("    nombre de données qui seront utilisées dans le calcul. Si l’une de ces options\n");
  printf("    n’est pas renseignée, alors le critère associé ne sera pas filtré et toutes les\n");
  printf("    données seront traitées (ex : si -g n’est pas renseignée, alors le programme\n");
  printf("    traitera TOUTES les longitudes)\n\n");
  printf("--> Les options -t, -p, -w, -h, et -m sont exclusives. Si l’une est présente, les autres\n");
  printf("    ne le sont pas.\n");
  printf("--> Les options peuvent être rentrées dans n’importe quel ordre.\n\n");
  printf("\n Aide détaillé du Script \n \n");
  printf("--> ARGUMENTS : \n");
  printf("    -F : (F)rance. -\n");
  printf("         permet de limiter les mesures à celles présentes en France\n");
  printf("         métropolitaine + Corse.\n\n");
  printf("    -G : (G)uyane. -\n");
  printf("         Permet de limiter les mesures à celles qui sont présentes en Guyane.\n\n");
  printf("    -S : (S)aint-Pierre et Miquelon. \n");
  printf("         Permet de limiter les mesures à celles qui sont présentes sur l’ile\n");
  printf("         située à l’Est du Canada.\n\n");
  printf("    -A : (A)ntilles. \n");
  printf("         Permet de limiter les mesures à celles qui sont présentes aux Antilles.\n\n");
  printf("    -O : (O)céan indien. \n");
  printf("         Permet de limiter les mesures à celles qui sont présentes dans\n");
  printf("         l’océan indien.\n\n");
  printf("    -Q : antarcti(Q)ue \n");
  printf("         Permet de limiter les mesures à celles qui sont présentes en antarctique.\n\n");
  printf("    -d <min> <max> : (d)ates. \n");
  printf("         Permet de filtrer les dates entre les valeurs moin et max, tout\n");
  printf("         comme le ferait le programme C.\n\n");
  printf("--> Les options -F, -G, -S, -A, -O, et -Q sont exclusives. Une seule option doit être\n");
  printf("    activée à la fois. Si plusieurs options sont activées, un message d’erreur\n");
  printf("    s’affichera.\n\n");

}

