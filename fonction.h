#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>
#include <unistd.h>
#ifndef FONCTION_H
#define FONCTION_H


/* Pierre  */
/* Résumé : structure d'une liste */
typedef struct maillon {
  float val;  // la valeur du maillon qui nous intéresse
  float dir;  // valeur direction du vent
  int heure;  // valeur heure
  int jour;   // valeur jour
  int mois;   // valeur mois 
  int annee;  // valeur année
  struct maillon* suivant;
} maillon;
typedef maillon* liste;

/* Pierre */
/* Résumé : structure d'une IDstation (tout ce qui ne change pas )*/
typedef struct point {
  int ID; // l'id de la station 
  int alt; // l'altitude
  int code; // code postal
  float latitude; 
  float longitude;
  maillon* station;
} point;
typedef point* IDstation;

/* Pierre et Antonin */
/* Résumé : structure d'un arbre */
typedef struct noeud {
  point* IDsta;
  struct noeud* filsG;
  struct noeud* filsD;
} noeud;
typedef noeud* arbre;

/* Pierre et Antonin */
/* Résumé : structure d'un point humidité */
typedef struct pointH { //structure qui aura la valeur d'humidite pour chaque  ID
  int ID;
  float lat;
  float lon;
  float donnee;
} pointH;
typedef pointH* humid;

/* Pierre et Antonin */
/* Résumé : structure d'un noeud humidité */
typedef struct noeudH {
  pointH* humid;
  struct noeudH* filsG;
  struct noeudH* filsD;
} noeudH;
typedef noeudH* arbreH;

/* Antonin  */
/* Résumé :structure que l'on va utilisé pour pression et température pour avoir le min le max et le moy*/
typedef struct pointT {
  int ID;
  float min;
  float max;
  float moy;
  float lat;
  float lon;
} pointT;
typedef pointT* temp;

/* Antonin */
/* Résumé : structure d'un point température */
typedef struct noeudT {
  pointT* temp;
  struct noeudT* filsG;
  struct noeudT* filsD;
} noeudT;
typedef noeudT* arbtemp;

/* Pierre */
/* Résumé : structure d'un point de température Date*/
typedef struct pointTD {
  int ID;
  float moy;
  int heure;
  int jour;
  int mois;
  int annee;
} pointTD;
typedef pointTD* tempD;

/* Pierre  */
/* Résumé : structure d'un noeud de température Date*/
typedef struct noeudTD {
  pointTD* temp;
  struct noeudTD* filsG;
  struct noeudT* filsD;
} noeudTD;
typedef noeudTD* arbtempD;

/* Gatien */
/* Résumé : structure d'un point hauteur */
typedef struct pointh {
  int ID;
  float lat;
  float lon;
  int donnee;
} pointh;
typedef pointh* altitude;

/* Gatien */
/* Résumé : structure d'un noeud hauteur */
typedef struct noeudh {
  pointh* altitude;
  struct noeudh* filsG;
  struct noeudh* filsD;
} noeudh;
typedef noeudh* arbreh;

/* Pierre */
/* Résumé : structure d'une liste date */
typedef struct maillond {
  float val;
  struct maillond* suivant;
} maillond;
typedef maillond* listed;

/* Pierre */
/* Résumé : structure d'un arbre d'heure*/
typedef struct noeudheure {
  maillond* liste;
  int heure;
  struct noeudheure* filsG;
  struct noeudheure* filsD;
} noeudheure;
typedef noeudheure* arbreheure;

/* Pierre */
/* Résumé : structure d'un arbre de jour*/
typedef struct noeudjour {
  noeudheure* arbh;
  int jour;
  struct noeudjour* filsG;
  struct noeudjour* filsD;
} noeudjour;
typedef noeudjour* arbrejour;

/* Pierre */
/* Résumé : structure d'un arbre de mois*/
typedef struct noeudmois {
  noeudjour* arbj;
  int mois;
  struct noeudmois* filsG;
  struct noeudmois* filsD;
} noeudmois;
typedef noeudmois* arbremois;

/* Pierre */
/* Résumé : structure d'un arbre d'année*/
typedef struct noeudannee {
  noeudmois* arbm;
  int annee;
  struct noeudannee* filsG;
  struct noeudannee* filsD;
} noeudannee;
typedef noeudannee* arbreannee;

/* Gatien */
/* Résumé : structure d'un point de vent*/
typedef struct pointV {
  int ID;
  float moyD; // moyenne de la direction
  float moyV; // moyenne de la vitesse
  float lat;
  float lon;
  float longa; // longitude de la fin de la fleche
  float lato;  // latitude de la fin de la fleche
} pointV;
typedef pointV* vent;

/* Gatien */
/* Résumé : structure d'un noeud de vent*/
typedef struct noeudV {
  pointV* vent;
  struct noeudV* filsG;
  struct noeudV* filsD;
} noeudV;
typedef noeudV* arbreV;





/* Pierre*/
/* Résumé : fonction qui vérifie si l'ID d'une station est déja dans l'arbre, et retourne cette ID */
IDstation verifID(int ID,arbre a,IDstation compt);


/* Pierre */
/* Résumé : fonction qui insert les données dans un arbre */
arbre insertionArbre (int ID,int alt,int code,float latitude,float longitude,float donnee,arbre a,int annee,int mois,int jour,int heure,float dir);

/* Pierre */
/* Résumé : fonction qui créer un noeud de différentes données */
noeud* creerNoeud(int ID,int alt,int code,float latitude,float longitude,float donnee,int annee,int mois,int jour,int heure,float dir);

/* Pierre */
/* Résumé : fonction qui cherche le min la station maximale*/
int maxStation (arbre a);

/* Pierre */
/* Résumé : fonction qui récupère les données de chaque colonnes d'un fichier et les range dans des arbres et des listes */
arbre lire(char* filename, int column, arbre a,int lon,int la,int da,float lonmin,float lonmax,float lamin,float lamax,int amin,int amax,int mmin,int mmax,int jmin,int jmax);

/* Pierre */
/* Résumé : fonction qui crée un maillon de différentes données*/
maillon* creerMaillon(float donnee,int annee,int mois,int jour,int heure,float dir);

/* Pierre */
/* Résumé : fonction qui crée un point de différentes données */
point* creerpoint(int ID,int alt,int code,float latitude,float longitude,float donnee,int annee,int mois,int jour,int heure,float dir);

/* Pierre */
/* Résumé : fonction qui afiiche les données qui se trouvent dans l'arbre */
void afficherarbre(arbre a);

/* Pierre */
/* Résumé : fonction qui affiche les données qui se trouvent dans la liste */
int afficherliste(liste l,int c);


/* Pierre et Antonin */
/* Résumé : fonction qui insère des valeurs dans une liste */
liste insertionliste(float val,liste l,int annee,int mois,int jour,int heure,float dir);

/* Pierre et Antonin */
/* Résumé : fonction qui crée l'arbre d'humidité*/
arbreH humidite(arbre a1,arbreH a2);

/* Antonin */
/* Résumé : fonction qui renvoie le max d'une liste */
float maxliste(liste l,float val);


/* Pierre */
/* Résumé : fonction qui free les mallocs crées pour un noeud */
void freenoeud(arbre a);

/* Pierre */
/* Résumé : fonction qui free les mallocs crées pour un point */
void freepoint(IDstation l);


/* Pierre */
/* Résumé : fonction qui free les mallocs crées pour un maillon */
void freemaillon(liste l);

/* Pierre et Antonin*/
/* Résumé : fonction qui crée un noeud d'humidité */
arbreH creerNoeudBis(int ID,float donnee,float lat,float lon);

/* Antonin */
/* Résumé : fonction qui insère une donnée d'humidité dans l'arbre humidité*/
arbreH insertionAH (int ID,float donnee,float lat,float lon,arbreH a);

/* Pierre et antonin */
/* Résumé : fonction qui crée un point d'humidité */
humid creerpointBis(int ID,float donnee,float lat,float lon);

/* Antonin */
/* Résumé : fonction qui free les mallocs crées pour un noeud d'humidité */
void freenoeudBis(arbreH a);

/* Antonin */
/* Résumé : fonction qui free les mallocs crées pour un point d'humidité */
void freepointBis(humid l);

/* Pierre */
/* Résumé : fonction qui affiche les valeur d'humidité qui se trouve dans l'arbre */
void afficherarbreBis(arbreH a);

/* Pierre */
/* Résumé : fonction qui renvoie la moyenne de la liste */
float moyliste(liste l);

/* Antonin */
/* Résumé : fonction qui renvoie le minimum de la liste */
float minliste(liste l,float val);

/* Antonin */
/* Résumé : fonction qui crée un point température (utilisé aussi pour pression1)*/
temp creerpointTemp(int ID,float min,float max,float moy,float lat,float lon);

/* Antonin */
/* Résumé : fonction qui crée un noeud température (utilisé aussi pour pression1)*/
arbtemp creerNoeudT(int ID,float min,float max, float moy,float lat,float lon);

/* Antonin */
/* Résumé : fonction qui free les mallocs crées pour un point température (utilisé aussi pour pression1) */
void freepointTemp(temp l);

/* Antonin */
/* Résumé : fonction qui free les mallocs crées pour un noeud température (utilisé aussi pour pression1)*/
void freenoeudTemp(arbtemp a);

/* Antonin */
/* Résumé : fonction qui insère le min et le max ainsi que la moyenne et les coordonées de température dans un arbre température (utilisé aussi pour pression1)*/
arbtemp insertionAT (int ID,float min,float max,float moy,float lat,float lon,arbtemp a);

/* Antonin */
/* Résumé : fonction qui affiche les données température qui se trouve dans l'arbre du même nom (utilisé aussi pour pression1) */
void afficherarbreTemp(arbtemp a);

/* Antonin */
/* Résumé : fonction qui prends les valeurs de a1 pour en faire un arbtemp */
arbtemp temp1(arbre a1, arbtemp a2);

/* Antonin */
/* Résumé : fonction qui écrit le continue de a dans un csv en appelant cacheecrireT  */
void ecrireT(arbtemp a,char* nom);

/* Antonin */
void cacheecrireT(arbtemp a,FILE* fichier);

/* Antonin */
/* Résumé : fonction qui prends les valeurs de a1 pour en faire un arbtemp pour pression*/
arbtemp pres1(arbre a1, arbtemp a2);

/* Antonin */
/* Résumé : fonction qui écrit le continue de a dans un csv en appelant cacheecrireT  */
void ecrireP(arbtemp a,char* nom);

/* Gatien */
/* Résumé : fonction qui écrit le continue de a dans un csv en appelant cacheecrireH  */
void ecrireH(arbreH a,char* nom);

/* Gatien */
/* Résumé : fonction qui écrit le continue de a dans un csv  */
void cacheecrireH(arbreH a,FILE* fichier);

/* Gatien */
/* Résumé : fonction qui prends a1 et qui en fais un arbreh */
arbreh hauteur(arbre a1,arbreh a2);

/* Gatien */
/* Résumé : fonction qui crée un noeud d'hauteur*/
arbreh creerNoeudBish(int ID,int donnee,float lat,float lon);

/* Gatien */
/* Résumé : fonction qui insère une donnée d'hauteur dans un arbre hauteur*/
arbreh insertionAh (int ID,int donnee,float lat,float lon,arbreh a);

/* Gatien */
/* Résumé : fonction qui crée un point d'hauteur*/
altitude creerpointBish(int ID,int donnee,float lat,float lon);

/* Gatien */
/* Résumé : fonction qui affiche l'abre hauteur*/
void afficherarbreBish(arbreh a);

/* Gatien */
/* Résumé : fonction qui écrit l'abre hauteur dans un csv en utilisant cacheecrireh*/
void ecrireh(arbreh a,char* nom);

/* Gatien */
/* Résumé : fonction qui écrit l'abre hauteur dans un csv */
void cacheecrireh(arbreh a,FILE* fichier);


/* Pierre*/
/* Résumé : fonction qui prend en entrée un arbre et renvoie un arbre d'année*/
arbreannee pres2(arbre a1,arbreannee a2);

/* Pierre*/
/* Résumé : fonction qui organise l'arbre de date en faisant appels aux foncions suivantes*/
arbreannee pres2cache(liste l,arbreannee a2);

/* Pierre*/
/* Résumé : fonction qui vérifie si la branche d'heure est déjà présente dans l'arbre */
arbreheure verifheure(int heure,arbreheure a,arbreheure compt);

/* Pierre*/
/* Résumé : fonction qui vérifie si la branche de jours est déjà présente dans l'arbre */
arbreheure verifjour(int jour,arbrejour a,arbreheure compt);

/* Pierre*/
/* Résumé : fonction qui vérifie si la branche de mois est déjà présente dans l'arbre */
arbrejour verifmois(int mois,arbremois a,arbrejour compt);

/* Pierre*/
/* Résumé : fonction qui vérifie si la branche d'année est déjà présente dans l'arbre */
arbremois verifann(int annee,arbreannee a,arbremois compt);

/* Pierre*/
/* Résumé : fonction qui insère les valeurs dans la liste de date*/
listed insertionlisted(int val,listed l);

/* Pierre*/
/* Résumé : fonction qui crée la liste des valeurs de date*/
listed creerMaillonD(float val);

/* Pierre*/
/* Résumé : fonction qui crée un noeud d'heure*/
arbreheure creerNoeudAheure(int heure,float val);

/* Pierre*/
/* Résumé : fonction qui crée un noeud de jour*/
arbrejour creerNoeudAjour(int jour,int heure,float val);

/* Pierre*/
/* Résumé : fonction qui crée un noeud de mois*/
arbremois creerNoeudAmois(int mois,int jour,int heure,float val);

/* Pierre*/
/* Résumé : fonction qui crée un noeud d'année*/
arbreannee creerNoeudAann(int annee,int mois,int jour,int heure,float val);

/* Pierre*/
/* Résumé : fonction qui insère la donnée d'heure dans un arbreheure*/
arbreheure insertionAheure(int heure,float val,arbreheure a);

/* Pierre*/
/* Résumé : fonction qui insère la donnée d'heure dans un arbrejour*/
arbrejour insertionAjour(int jour,int heure,float val,arbrejour a);

/* Pierre*/
/* Résumé : fonction qui insère la donnée d'heure dans un arbremois*/
arbremois insertionAmois(int mois,int jour,int heure,float val,arbremois a);

/* Pierre*/
/* Résumé : fonction qui insère la donnée d'heure dans un arbreannee*/
arbreannee insertionAann(int annee,int mois,int jour,int heure,float val,arbreannee a);

/* Pierre*/
/* Résumé : fonction qui affiche les arbres d'années*/
void afficherarbreAnn(arbreannee a);

/* Pierre*/
/* Résumé : fonction qui affiche les arbres de mois*/
void afficherarbremois(arbremois a);

/* Pierre*/
/* Résumé : fonction qui affiche les arbres de jours*/
void afficherarbrejour(arbrejour a);

/* Pierre*/
/* Résumé : fonction qui affiche les arbres d'heure*/
void afficherarbreheure(arbreheure a);

/* Pierre*/
/* Résumé : fonction qui affiche les valeurs de la listes de date*/
void afficherarbrelisted(listed a);

/* Pierre*/
/* Résumé : fonction qui fait appel aux autres écrireT2*/
void ecrireT2(arbreannee a,char* nom);

/* Pierre*/
/* Résumé : fonction qui envoie les branches et les années correspondantes dans ecrireT2mois*/
void ecrireT2ann(arbreannee a,FILE* fichier);

/* Pierre*/
/* Résumé : fonction qui envoie les branches,les années et les mois correspondantes dans ecrireT2jour*/
void ecrireT2mois(int annee,arbremois a,FILE* fichier);

/* Pierre*/
/* Résumé : fonction qui envoie les branches,les années, les mois et les jours correspondantes dans ecrireT2heure*/
void ecrireT2jour(int mois,int annee,arbrejour a,FILE* fichier);

/* Pierre*/
/* Résumé : fonction qui écrit les données de dates dans un csv*/
void ecrireT2heure(int jour,int mois,int annee,arbreheure a,FILE* fichier);

/* Pierre*/
/* Résumé : fonction qui renvoie la moyenne des listes de valeurs pour une même date*/
float moylisted(listed l);



/* Gatien */
/* Résumé : fonction qui créer un point de vent */
vent creerpointV(int ID,float moyD,float moyV,float lat,float lon,float longa,float lato);

/* Gatien*/
/* Résumé : fonction qui creer un noeud de vent */
arbreV creerNoeudV(int ID,float moyD,float moyV,float lat,float lon,float longa,float lato);

/* Gatien  */
/* Résumé : fonction qui insère une donnée de vent dans l'arbre de vent*/
arbreV insertionAV (int ID,float moyD,float moyV,float lat,float lon,float longa,float lato,arbreV a);

/* Gatien et Antonin*/
/* Résumé : fonction qui prend en entrée l'arbre a1 et renvoie un arbreV en sortie*/
arbreV souffle(arbre a1, arbreV a2);

/* Gatien */
/* Résumé : fonction qui écrit l'abreV dans un csv en utilisant cacheecrireV*/
void ecrireV(arbreV a,char* nom);

/* Gatien */
/* Résumé : fonction qui écrit l'abreV dans un csv */
void cacheecrireV(arbreV a,FILE* fichier);

/* Gatien */
/* Résumé : fonction qui renvoie la moyenne des vitesses de vent*/
float moylisteBis(liste l);

/* Gatien et Antonin */
/* Résumé : fonction qui renvoie la moyenne des x de la direction de vent*/
float moylistex(liste l);

/*Mathis*/
/* Résumé : fonction help qui explique le fonctionnement du programme et du script*/
void help();

#endif
